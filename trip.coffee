((root, factory)->
  if typeof define is 'function' and define.amd
    define(['jquery'], factory)
  else
    root.TripJS = factory root.$
)(@, ($)->
  win = window
  doc = document
  $doc = $ doc
  $win = $ win
  $body = null

  dummyHelperTemplate = (slide, options)->
    """
    <div class='tripjs-helper-container'>
      <div class='tripjs-helper-title'>#{slide.title or ''}</div>
      <div class='tripjs-helper-body'>#{slide.body or ''}</div>
      <div class='tripjs-helper-actions'>
        <div class='tripjs-helper-skip' data-tripjs-action='skip'>#{options.buttons.skip}</div>
        <div class='tripjs-helper-prev' data-tripjs-action='prev'>#{options.buttons.prev}</div>
        <div class='tripjs-helper-next' data-tripjs-action='next'>#{options.buttons.next}</div>
        <div class='tripjs-helper-done' data-tripjs-action='done'>#{options.buttons.done}</div>
      </div>
    </div>
    """

  class @TripJS
    
    on: $.fn.on
    off: $.fn.off
    trigger: $.fn.trigger
    each: $.fn.each

    next: ->
      @currentIndex++
      if @currentIndex >= @slides.length
        @done()
      else
        @play()
      undefined
    
    prev: ->
      @currentIndex--
      if @currentIndex < 0
        @done()
      else
        @play()
      undefined
    
    skip: ->
      @close()
      @trigger "skip"
      undefined

    done: ->
      @close()
      @trigger "done"
      undefined

    close: ->
      wrapper = $(".tripjs-wrapper")
      wrapper.appendTo($("body"))
      wrapper.children().unwrap()
      @el.remove()
      $body.css "overflow", ""
      if @_bindKeyboardEvents
        $doc.off "keyup", @keyBoardHandler
      undefined
    
    constructor: (options = {})->
      @_helperTemplate = options.helperTemplate or dummyHelperTemplate
      @_bindKeyboardEvents = options.bindKeyboardEvents or true
      @_buttonsLabels = 
        prev: options.prevLabel or "Prev"
        next: options.nextLabel or "Next"
        done: options.doneLabel or "Done"
        skip: options.skipLabel or "Skip"
      @_scrollTiming = options.scrollTiming or 500
      @slides = options.slides or []
      $body = $ "body"
      @

    initialize: ->
      @isInitialized = true
      @el = tripElement = $ """
      <div class='tripjs'>
        <div class='tripjs-blured-overlay'>
          <div class='tripjs-overlay'></div>
          <div class='tripjs-colored-overlay'></div>
        </div>
        <div class='tripjs-container'>
          <div class='tripjs-content'></div>
          <div class='tripjs-helper'></div>
        </div>
      </div>
      """
      @helper = helper = tripElement.find '.tripjs-helper'
      @container = container = tripElement.find '.tripjs-container'
      @content = tripElement.find('.tripjs-content')
      tripElement.on "click", "[data-tripjs-action='next']", => 
        @next()
        true
      tripElement.on "click", "[data-tripjs-action='prev']", => 
        @prev()
        true
      tripElement.on "click", "[data-tripjs-action='done']", =>
        @done()
        true
      tripElement.on "click", "[data-tripjs-action='skip']", =>
        @skip()
        true  
      if @_bindKeyboardEvents
        @keyBoardHandler = (e)=>
          switch e.keyCode
            # esc
            when 27
              if @currentIndex is @slides.length
                @done()
              else
                @skip()
              false
            # left
            when 37
              @prev()
              false
            # right
            when 39
              @next()
              false
            else
              true
        $doc.on "keyup", @keyBoardHandler
      $body
      .wrapInner("<div class='tripjs-wrapper'></div>")
      .append tripElement
      $body.css "overflow", "hidden"
      $('.tripjs-wrapper').appendTo(tripElement.find('.tripjs-overlay'))
      @

    # play currentSlide if present?
    play: ->
      if (typeof @currentIndex isnt "number") or not (@currentSlide = currentSlide = @slides[@currentIndex])
        return false
      @trigger "slide", @currentIndex
      $target = $ currentSlide.el
      target = $target[0]
      $targetClone = $target.clone()

      targetOffset = $target.offset()
      top = targetOffset.top
      left = targetOffset.left
      callback = =>
        @container
        .css
          "width": $target.width()
          "height": $target.height()
          "top": top
          "left": left
        styles = getComputedStyle(target)
        
        $.extend($targetClone[0].style, styles)

        helperTemplate = currentSlide.helperTemplate or @_helperTemplate
        @content.html $targetClone.addClass('tripjs-active-slide')
        @helper.html(helperTemplate
          body: currentSlide.body
          title: currentSlide.title
        ,
          buttons: @_buttonsLabels
        )
        .removeClass("top left right bottom")
        .addClass(currentSlide.helperPosition || "bottom")
        .toggleClass('tripjs-first-slide', @currentIndex is 0)
        .toggleClass('tripjs-last-slide', @currentIndex is @slides.length-1)
        @el.show()

      if top <= win.innerHeight
        scrollTop = top
      else if top + win.innerHeight >= $doc.height
        scrollTop = $doc.height - win.innerHeight
      else
        scrollTop = top
      $body.animate
        "scrollTop": scrollTop
      , @_scrollTiming, callback

      true

    start: ->
      unless @isInitialized
        @initialize()
      @currentIndex = 0
      @play()
      undefined
)
